﻿namespace Algoritms
{
    internal class InsertionSort
    {
        /// <summary>
        /// InsertionSort
        /// </summary>
        /// <param name="lijst"></param>
        /// <returns></returns>
        public int[] Sort(int[] lijst)
        {
            int i, j, newValue;
            for (i = 1; i < lijst.Length; i++)
            {
                newValue = lijst[i];
                j = i;
                while (j > 0 && lijst[j - 1] > newValue)
                {
                    lijst[j] = lijst[j - 1];
                    j--;
                }
                lijst[j] = newValue;
            }
            return lijst;
        }
    }
}