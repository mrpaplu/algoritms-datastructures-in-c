﻿using System;

namespace Algoritms
{
    internal class QuickSort
    {
        public static void Sort(IComparable[] items)
        {
            Sort(items, 0, items.Length - 1);
        }

        private static void Sort(IComparable[] items, int low, int high)
        {
            int i = low, j = high;
            IComparable middle = items[(low + high) / 2];
            while (i <= j)
            {
                while (items[i].CompareTo(middle) < 0)
                {
                    i++;
                }

                while (items[j].CompareTo(middle) > 0)
                {
                    j--;
                }

                if (i <= j)
                {
                    IComparable tmp = items[i];
                    items[i] = items[j];
                    items[j] = tmp;

                    i++;
                    j--;
                }
            }
            if (low < j)
            {
                Sort(items, low, j);
            }
            if (i < high)
            {
                Sort(items, i, high);
            }
        }
    }
}