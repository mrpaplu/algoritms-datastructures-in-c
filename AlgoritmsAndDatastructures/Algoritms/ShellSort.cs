﻿namespace Algoritms
{
    internal class ShellSort
    {
        /// <summary>
        /// Shellsort
        /// </summary>
        /// <param name="lijst"></param>
        /// <returns></returns>
        public int[] Sort(int[] lijst)
        {
            int i, j, increment, temp;
            increment = 3;
            while (increment > 0)
            {
                for (i = 0; i < lijst.Length; i++)
                {
                    j = i;
                    temp = lijst[i];
                    while ((j >= increment) && (lijst[j - increment] > temp))
                    {
                        lijst[j] = lijst[j - increment];
                        j = j - increment;
                    }
                    lijst[j] = temp;
                }
                if (increment / 2 != 0)
                {
                    increment = increment / 2;
                }
                else if (increment == 1)
                {
                    increment = 0;
                }
                else
                {
                    increment = 1;
                }
            }
            return lijst;
        }
    }
}