﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algoritms
{
    class Program
    {
        static int[] arr = { 8, 1, 4, 1, 5, 9, 2, 6, 5 };
        int maxIndex = arr.Count();

        static void Main(string[] args)
        {
            Console.WriteLine("MergeSort non-recursive:");
            Console.WriteLine();
            Console.WriteLine("Unsorted");
            foreach (var i in arr)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();
            Console.WriteLine("Sorted");
            MergeSort.MergeSort_Recursive(arr, 0, arr.Count() - 1);
            foreach (var i in arr)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("MergeSort non-recursive:");
            MergeSorter oSorter = new MergeSorter();
            ArrayList arrayUnsorted = new ArrayList();
            arrayUnsorted.Add(1);
            arrayUnsorted.Add(33);
            arrayUnsorted.Add(4);
            arrayUnsorted.Add(43);
            //--------------------------------------
            //Sort
            //--------------------------------------
            ArrayList arraySorted = oSorter.MergeSort(arrayUnsorted);
            //--------------------------------------
            //Print
            //--------------------------------------
            foreach (int i in arraySorted)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();
        }
    }
}
