﻿using System;

namespace Datastructures.Graphs.Internal
{
    class Path : IComparable<Path>
    {
        public Vertex Dest;
        public double Cost;

        public Path(Vertex d, double c)
        {
            Dest = d;
            Cost = c;
        }

        public int CompareTo(Path rhs)
        {
            double otherCost = rhs.Cost;
            return Cost < otherCost ? -1 : Cost > otherCost ? 1 : 0;
        }
    }
}
