﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP_AD_2013_S1038902_KoenClaassen
{
    class BinaryNode
    {
        public int Element;
        public BinaryNode Left;
        public BinaryNode Right;

        public BinaryNode(int x)
        {
            Element = x;
            Left = null;
            Right = null;
        }

        public override string ToString()
        {
            return Element + " ";
        }
    }
}
