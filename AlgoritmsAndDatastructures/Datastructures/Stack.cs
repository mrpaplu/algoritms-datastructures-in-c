﻿using System;

namespace Datastructures
{
    internal class Stack<T> where T : IComparable
    {
        // CONSTRUCTION: with no initializer
        //
        // ******************PUBLIC OPERATIONS*********************
        // void push( x )         --> Insert x
        // void pop( )            --> Remove most recently inserted item
        // AnyType top( )         --> Return most recently inserted item
        // AnyType topAndPop( )   --> Return and remove most recent item
        // boolean isEmpty( )     --> Return true if empty; else false
        // void makeEmpty( )      --> Remove all items
        // ******************ERRORS********************************
        // top, pop, or topAndPop on empty stack

        private Node<T> _topOfStack;

        public Stack()
        {
            _topOfStack = null;
        }

        /**
         * Test if the stack is logically empty.
         * @return true if empty, false otherwise.
         */

        public bool IsEmpty()
        {
            return _topOfStack == null;
        }

        /**
         * Make the stack logically empty.
         */

        public void MakeEmpty()
        {
            _topOfStack = null;
        }

        /**
         * Insert a new item into the stack.
         * @param x the item to insert.
         */

        public void Push(T x)
        {
            _topOfStack = new Node<T>(x, _topOfStack);
        }

        /**
         * Remove the most recently inserted item from the stack.
         * @throws UnderflowException if the stack is empty.
         */

        public void Pop()
        {
            if (IsEmpty())
            {
                throw new Exception("ListStack pop");
            }
            _topOfStack = _topOfStack.Next;
        }

        /**
         * Get the most recently inserted item in the stack.
         * Does not alter the stack.
         * @return the most recently inserted item in the stack.
         * @throws UnderflowException if the stack is empty.
         */

        public T Top()
        {
            if (IsEmpty())
            {
                throw new Exception("ListStack top");
            }
            return _topOfStack.Value;
        }

        /**
         * Return and remove the most recently inserted item
         * from the stack.
         * @return the most recently inserted item in the stack.
         * @throws UnderflowException if the stack is empty.
         */

        public T TopAndPop()
        {
            if (IsEmpty())
            {
                throw new Exception("ListStack topAndPop");
            }

            T topItem = _topOfStack.Value;
            _topOfStack = _topOfStack.Next;
            return topItem;
        }
    }
}