﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datastructures
{
    class BinaryNode<T> : Node<T> where T : IComparable
    {
        public BinaryNode<T> Left { get; set; }
        public BinaryNode<T> Right { get; set; }

        public BinaryNode()
            : base()
        {

        }

        public BinaryNode(T data)
            : base(data, null)
        {

        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (Left != null)
                sb.Append(Left.ToString());

            sb.Append(Value);

            if (Right != null)
                sb.Append(Right.ToString());


            return sb.ToString();
        }
    }
}
