﻿using System;
using Datastructures.Lists.Interfaces;

namespace Datastructures.Lists
{
    class MyLinkedList<T> : ILinkedList<T>
    {
        private Node<T> Header { get; set; }

        public MyLinkedList()
        {
            Header = new Node<T>(default(T));
        } 

        /// <summary>
        /// Add an item to the beginning of the list
        /// </summary>
        /// <param name="data"></param>
        public void AddFirst(T data)
        {
            Node<T> newNode = new Node<T>(data) {Next = Header.Next};
            Header.Next = newNode;
        }

        /// <summary>
        /// Clear the LinkedList
        /// </summary>
        public void Clear()
        {
            Header.Next = null;
        }

        /// <summary>
        /// Print all values in the list
        /// </summary>
        public void Print()
        {
            Node<T> current = Header;
            while (current.Next != null)
            {
                current = current.Next;
                Console.WriteLine(current.Value);
            }
        }

        /// <summary>
        /// Insert a value at a certain index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="data"></param>
        public void Insert(int index, T data)
        {
            Node<T> current = Header;
            for (int i = 0; i < index - 1; i++)
            {
                current = current.Next;
            }
            Node<T> newNode = new Node<T>(data){ Next = current.Next };
            current.Next = newNode;
        }

        /// <summary>
        /// Remove the first item of the LinkedList
        /// </summary>
        public void RemoveFirst()
        {
            Header.Next = Header.Next.Next;
        }

        /// <summary>
        /// Return the value of the first item of the list
        /// </summary>
        /// <returns></returns>
        public T GetFirst()
        {
            return Header.Next.Value;
        }

        /// <summary>
        /// A node holds a value in the LinkedList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        class Node<T>
        {
            public T Value { get; set; }
            public Node<T> Next { get; set; } 

            public Node(T value)
            {
                Value = value;
            } 
        }
    }
}
