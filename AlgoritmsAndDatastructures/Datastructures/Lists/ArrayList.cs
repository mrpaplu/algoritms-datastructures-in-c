﻿using System;
using Datastructures.Lists.Interfaces;

namespace Datastructures.Lists
{
    class ArrayList<T> : IArrayList<T>
    {
        private T[] Values { get; set; }
        private int Index { get; set; }

        public ArrayList()
        {
            Initialize();
        } 

        /// <summary>
        /// Initialize (or reset) our ArrayList
        /// </summary>
        private void Initialize()
        {
            Values = new T[10];
            Index = 0;
        }

        /// <summary>
        /// Add a value to the ArrayList. If arraylist is full, double it.
        /// </summary>
        /// <param name="value"></param>
        public void Add(T value)
        {
            if (Index + 1 > Values.Length)
            {
                DoubleLength();
            }
            Values[Index] = value;
            Index++;
        }

        /// <summary>
        /// Double the lenth of the array
        /// </summary>
        private void DoubleLength()
        {
            T[] oldValues = Values;
            Values = new T[Values.Length * 2];

            for (int i = 0; i < oldValues.Length; i++)
            {
                Values[i] = oldValues[i];
            }
        }

        /// <summary>
        /// Return the value at a given index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T Get(int index)
        {
            return Values[index - 1];
        }

        /// <summary>
        /// Set the value of a given index, if the index's higher then 
        /// the length of Values, double array and recursively call Set again
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public void Set(int index, T value)
        {
            if (index > Values.Length)
            {
                DoubleLength();
                Set(index, value);
            }
            else
            {
                Values[index - 1] = value;
            }
        }

        /// <summary>
        /// Write every value to console
        /// </summary>
        public void Print()
        {
            for (int i = 0; i < Index; i++ )
            {
                Console.WriteLine(Values[i]);
            }
        }

        /// <summary>
        /// Clear all values of this ArrayList
        /// </summary>
        public void Clear()
        {
            Initialize();
        }

        public int CountOccurences(T value)
        {
            throw new NotImplementedException();
        }
    }
}
