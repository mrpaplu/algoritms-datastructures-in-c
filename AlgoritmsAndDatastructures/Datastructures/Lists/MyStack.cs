﻿using Datastructures.Lists.Interfaces;

namespace Datastructures.Lists
{
    class MyStack<T> : IStack<T>
    {
        private ILinkedList<T> Values { get; set; }
 
        public MyStack()
        {
            Values = new MyLinkedList<T>();
        } 
        
        public void Push(T value)
        {
            Values.AddFirst(value);
        }

        public T Top()
        {
            return Values.GetFirst();
        }

        public T Pop()
        {
            T value = Values.GetFirst();
            Values.RemoveFirst();
            return value;
        }

        public void Print()
        {
            Values.Print();
        }
    }
}
