﻿using System;
using Datastructures.Lists.Interfaces;

namespace Datastructures.Lists
{
    class DoublyLinkedList<T> : IDoublyLinkedList<T>
    {
        private Node<T> Header { get; set; }
        private Node<T> Footer { get; set; }
        private int Size { get; set; }

        public DoublyLinkedList()
        {
            Header = new Node<T>(default(T));
            Footer = new Node<T>(default(T));
            Header.Next = Footer;
            Footer.Previous = Header;
            Size = 0;
        } 

        /// <summary>
        /// Add an item to the beginning of the list
        /// </summary>
        /// <param name="data"></param>
        public void AddFirst(T data)
        {
            Node<T> newNode = new Node<T>(data)
            {
                Next = Header.Next,
                Previous = Header
            };
            Header.Next.Previous = newNode;
            Header.Next = newNode;
            Size++;
        }

        public void AddLast(T data)
        {
            Node<T> newNode = new Node<T>(data)
            {
                Previous = Footer.Previous,
                Next = Footer
            };
            Footer.Previous.Next = newNode;
            Footer.Previous = newNode;
            Size++;
        }

        /// <summary>
        /// Clear the LinkedList
        /// </summary>
        public void Clear()
        {
            Header.Next = null;
        }

        /// <summary>
        /// Print all values in the list
        /// </summary>
        public void Print()
        {
            Node<T> current = Header;
            for (int i = 0; i < Size; i++ )
            {
                current = current.Next;
                Console.WriteLine(current.Value);
            }
        }

        /// <summary>
        /// Insert a value at a certain index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="data"></param>
        public void Insert(int index, T data)
        {
            Node<T> current = Header;
            for (int i = 0; i < index - 1; i++)
            {
                current = current.Next;
            }
            Node<T> newNode = new Node<T>(data)
            {
                Next = current.Next,
                Previous = current
            };
            current.Next.Previous = newNode;
            current.Next = newNode;
            Size++;
        }

        /// <summary>
        /// Remove the first item of the LinkedList
        /// </summary>
        public void RemoveFirst()
        {
            Header.Next = Header.Next.Next;
            Size--;
        }

        public void RemoveLast()
        {
            Footer.Previous = Footer.Previous.Previous;
            Size--;
        }

        /// <summary>
        /// Return the value of the first item of the list
        /// </summary>
        /// <returns></returns>
        public T GetFirst()
        {
            return Header.Next.Value;
        }

        public T GetLast()
        {
            return Footer.Previous.Value;
        }

        /// <summary>
        /// A node holds a value in the LinkedList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        class Node<T>
        {
            public T Value { get; set; }
            public Node<T> Next { get; set; }
            public Node<T> Previous { get; set; } 

            public Node(T value)
            {
                Value = value;
            } 
        }
    }
}
