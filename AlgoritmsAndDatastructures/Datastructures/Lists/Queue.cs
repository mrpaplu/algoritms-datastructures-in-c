﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datastructures.Lists
{
    class Queue<T>
    {
        /// <summary>
        /// De voordelen zijn je hoeft niet te kutten met counters.
        /// Nadelen? q.q ??
        /// </summary>
        private List<T> TheArray;

        public Queue()
        {
            TheArray = new List<T>();
            MakeEmpty();
        }

        public bool IsEmpty()
        {
            return TheArray.Count == 0;
        }

        public void MakeEmpty()
        {
            TheArray.Clear();
        }

        public T Dequeue()
        {
            if (IsEmpty())
                Console.WriteLine("not good, nono");

            T first = TheArray.First();
            TheArray.Remove(first);

            return first;
        }

        public T GetFront()
        {
            return TheArray.First();
        }

        public void Enqueue(T val)
        {
            TheArray.Add(val);
        }
    }
}
