﻿namespace Datastructures.Lists.Interfaces
{
    interface ILinkedList<T>
    {
        // voeg een item toe aan het begin van de lijst
        void AddFirst(T data);

        void Clear();

        void Print();

        // voeg een item in op een bepaalde index (niet 	
        // overschrijven!)
        void Insert(int index, T data);

        // verwijder het eerste item
        void RemoveFirst();

        // geeft het eerste item terug
        T GetFirst();
    }
}
