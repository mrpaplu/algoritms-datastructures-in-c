﻿namespace Datastructures.Lists.Interfaces
{
    interface IStack<T>
    {
        // Add value to the stack
        void Push(T value);

        // Return top value
        T Top();

        // Return and remove top value
        T Pop();

        // Print values
        void Print();
    }
}
