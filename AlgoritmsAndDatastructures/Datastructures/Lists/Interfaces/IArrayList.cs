﻿namespace Datastructures.Lists.Interfaces
{
    internal interface IArrayList<T>
    {
        // toevoegen aan het einde van de lijst, mits niet vol
        void Add(T value);

        // haal de waarde op van een bepaalde index
        T Get(int index);

        // wijzig een item op een bepaalde index
        void Set(int index, T value);

        // print de inhoud van de list
        void Print();

        // maak de list leeg
        void Clear();

        // tel hoe vaak het gegeven getal voorkomt
        int CountOccurences(T value);
    }
}