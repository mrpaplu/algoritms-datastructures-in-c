﻿namespace Datastructures.Lists.Interfaces
{
    interface IDoublyLinkedList<T>
    {
        // voeg een item toe aan het begin van de lijst
        void AddFirst(T data);
        void AddLast(T data);

        void Clear();

        void Print();

        // voeg een item in op een bepaalde index (niet 	
        // overschrijven!)
        void Insert(int index, T data);

        // verwijder het eerste item
        void RemoveFirst();
        void RemoveLast();

        // geeft het eerste item terug
        T GetFirst();
        T GetLast();
    }
}
