﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Datastructures.Graphs;
using Datastructures.Graphs.Internal;
using Datastructures.Trees;

namespace Datastructures
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Uitwerking opdracht 1:");
            Opdracht1();
            Console.WriteLine();

            Console.WriteLine("Uitwerking opdracht 2:");
            Opdracht2();
            Console.WriteLine();

            Console.WriteLine("Uitwerking opdracht 3:");
            Opdracht3();
            Console.WriteLine();

            Console.WriteLine("Uitwerking opdracht 4:");
            Opdracht4();

            Console.ReadLine();
        }

        static void Opdracht4()
        {
            //Opdracht 4a
            Graph graph = new Graph();

            Vertex A = new Vertex("A");
            Vertex B = new Vertex("B");
            Vertex C = new Vertex("C");
            Vertex D = new Vertex("D");
            Vertex E = new Vertex("E");
            Vertex F = new Vertex("F");
            Vertex G = new Vertex("G");
            Vertex H = new Vertex("H");
            Vertex K = new Vertex("K");

            graph.AddEdge("A", "B");
            graph.AddEdge("C", "B");
            graph.AddEdge("E", "C");
            graph.AddEdge("H", "E");
            graph.AddEdge("H", "K");
            graph.AddEdge("K", "G");
            graph.AddEdge("G", "H");
            graph.AddEdge("G", "F");
            graph.AddEdge("F", "F");
            graph.AddEdge("D", "F");
            graph.AddDoubleEdge("D", "G");

            //Opdracht 4b
            graph.VulAfstand("G");
            graph.ToonAfstand("G");
            Console.WriteLine();

            graph.ToonCycles();

            //Opdracht 4c
            //Console.WriteLine("HasCycle op E");
            //Console.WriteLine(graph.HasCycle("E"));
            //Console.WriteLine("HasCycle op K");
            //Console.WriteLine(graph.HasCycle("K"));
        }

        static void Opdracht3()
        {
            BinaryHeap tree1 = MakeTreeOne();
            BinaryHeap tree2 = MakeTreeTwo();
            BinaryHeap tree3 = MakeTreeThree();

            Console.WriteLine("isComplete en isMaxHeap en isMin en isPerfect van tree1:");
            Console.WriteLine(tree1.IsComplete());
            Console.WriteLine(tree1.IsMaxHeap());
            Console.WriteLine(tree1.IsMinHeap());
            Console.WriteLine(tree1.IsPerfect());
            Console.WriteLine(tree1.PrintBreadthFirst());
            Console.WriteLine();
            Console.WriteLine("isComplete en isMaxHeap en isMin en isPerfect van tree2:");
            Console.WriteLine(tree2.IsComplete());
            Console.WriteLine(tree2.IsMaxHeap());
            Console.WriteLine(tree2.IsMinHeap());
            Console.WriteLine(tree2.IsPerfect());
            Console.WriteLine(tree2.PrintBreadthFirst());
            Console.WriteLine();
            Console.WriteLine("isComplete en isMaxHeap en isMin en isPerfect van tree3:");
            Console.WriteLine(tree3.IsComplete());
            Console.WriteLine(tree3.IsMaxHeap());
            Console.WriteLine(tree3.IsMinHeap());
            Console.WriteLine(tree3.IsPerfect());
            Console.WriteLine(tree3.PrintBreadthFirst());
            Console.WriteLine();
        }

        static void Opdracht2()
        {
            //Maak de boom
            BinarySearchTree tree = new BinarySearchTree();
            int[] insert = { 6, 8, 2, 1, 4, 3 };
            foreach (int i in insert)
                tree.Insert(i);
            Console.WriteLine("Test op volledige boom, verwacht antwoord 2");
            Console.WriteLine(tree.GeefEenNaKleinsteElement().Element);

            tree.Remove(1);
            Console.WriteLine("Test op boom zonder 1, verwacht antwoord 3");
            Console.WriteLine(tree.GeefEenNaKleinsteElement().Element);
            Console.WriteLine("Inorder");
            Console.WriteLine(tree.PrintInOrder());
            Console.WriteLine("Postorder");
            Console.WriteLine(tree.PrintPostOrder());
            Console.WriteLine("Preorder");
            Console.WriteLine(tree.PrintPreOrder());
        }

        static void Opdracht1()
        {
            Opdracht1 opdr1 = new Opdracht1();

            Console.WriteLine("PrintLetters(3)");
            opdr1.PrintLetters(3);
            Console.WriteLine();

            Console.WriteLine("PrintLetters(0)");
            opdr1.PrintLetters(0);
            Console.WriteLine();

            Console.WriteLine("PrintLetters2(3,5)");
            opdr1.PrintLetters2(3, 5);
            Console.WriteLine();

            Console.WriteLine("PrintLetters2(2,0)");
            opdr1.PrintLetters2(2, 0);
            Console.WriteLine();
        }

        //eerste boom van opdracht3
        static BinaryHeap MakeTreeOne()
        {
            BinaryHeap tree = new BinaryHeap();
            tree.Data[1] = 10;
            tree.Data[2] = 4;
            tree.Data[3] = 7;
            tree.Data[4] = 1;
            tree.Data[5] = 3;
            tree.Data[7] = 5;

            tree.CurrentSize = 6;
            return tree;
        }
        //tweede boom van opdracht3
        static BinaryHeap MakeTreeTwo()
        {
            BinaryHeap tree = new BinaryHeap();
            tree.Data[1] = 15;
            tree.Data[2] = 5;
            tree.Data[3] = 11;
            tree.Data[4] = 3;
            tree.Data[5] = 4;
            tree.Data[6] = 10;
            tree.Data[7] = 7;
            tree.Data[8] = 1;
            tree.CurrentSize = 8;

            Console.Write("preorder: ");
            tree.PrintPreOrder(1);
            Console.WriteLine();

            Console.Write("InOrder: ");
            tree.PrintInOrder(1);
            Console.WriteLine();

            Console.Write("PostOrder: ");
            tree.PrintPostOrder(1);
            Console.WriteLine();
            return tree;
        }

        static BinaryHeap MakeTreeThree()
        {
            BinaryHeap tree = new BinaryHeap();
            tree.Data[1] = 1;
            tree.Data[2] = 5;
            tree.Data[3] = 2;
            tree.Data[4] = 6;
            tree.Data[5] = 9;
            tree.Data[6] = 4;
            tree.Data[7] = 3;
            tree.Data[8] = 10;
            tree.Data[9] = 11;

            tree.CurrentSize = 8;
            return tree;
        }
    }
}
