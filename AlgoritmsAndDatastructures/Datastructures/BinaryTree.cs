﻿using System;

namespace Datastructures
{
    public class BinaryTree<T> where T : IComparable
    {
        private BinaryNode<T> _root;

        public static BinaryTree<string> CreateTestTree()
        {
            BinaryTree<string> tree = new BinaryTree<string>();
            tree._root = new BinaryNode<string>();
            tree._root.Value = "a";
            tree._root.Left = new BinaryNode<string>();
            tree._root.Right = new BinaryNode<string>();
            tree._root.Left.Value = "b";
            tree._root.Right.Value = "c";
            tree._root.Right.Left = new BinaryNode<string>();
            tree._root.Right.Right = new BinaryNode<string>();
            tree._root.Right.Right.Value = "e";
            tree._root.Right.Left.Value = "d";

            return tree;
        }

        public override string ToString()
        {
            if (_root != null)
            {
                return _root.ToString();
            }
            return "lege boom";
        }
    }
}