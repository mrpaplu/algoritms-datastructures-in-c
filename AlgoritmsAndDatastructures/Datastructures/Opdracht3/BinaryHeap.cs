﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP_AD_2013_S1038902_KoenClaassen
{
    class BinaryHeap
    {
        //Opgave 3 Toetspracticum AD
        //isMaxHeap
        public bool IsMaxHeap()
        {
            bool maxHeap = true;
            for (int i = 1; i <= CurrentSize / 2; i++)
            {
                if (Data[i] < Data[i * 2] || Data[i] < Data[i * 2 + 1])
                {
                    maxHeap = false;
                }
            }
            return maxHeap;
        }

        public bool IsMinHeap()
        {
            bool minHeap = true;
            for (int i = 1; i <= CurrentSize / 2; i++)
            {
                if ((Data[i] > Data[i * 2] && Data[i * 2] != 0) || (Data[i] > Data[i * 2 + 1] && Data[i * 2 + 1] != 0))
                {
                    minHeap = false;
                }
            }
            return minHeap;
        }

        //isComplete (is a heap)
        public bool IsComplete()
        {
            bool completeTree = true;
            for (int i = 1; i <= CurrentSize; i++)
            {
                if (Data[i] == 0)
                {
                    completeTree = false;
                }
            }
            return completeTree;
        }

        public bool IsPerfect()
        {
            bool isPerfectTree = true;
            for (int i = 1; i <= CurrentSize / 2 ; i++)
            {
                if (Data[i * 2] == 0 || Data[i * 2 + 1] == 0)
                {
                    isPerfectTree = false;
                }
            }
            return isPerfectTree;
        }

        public int CurrentSize;
        public int[] Data = new int[10];

        public void Add(int x)
        {
            if (CurrentSize + 1 == Data.Length)
                DoubleArray();

            // Percolate up
            int hole = ++CurrentSize;
            Data[0] = x;

            for (; x.CompareTo(Data[hole / 2]) < 0; hole /= 2)
                Data[hole] = Data[hole / 2];
            Data[hole] = x;
        }
        
        public String PrintBreadthFirst()
        {
            //NLR
            String s = "";
            if (CurrentSize > 0)
            {
                s += Data[1];

                for (int i = 1; i <= CurrentSize/2; i++)
                {
                    s += " " + Data[i * 2];

                    if ((i * 2 + 1) <= CurrentSize)
                        s += " " + Data[i * 2 + 1];
                }

                return s;
            }
            else
                return "De heap is leeg!";
        }

        private void DoubleArray()
        {
            int[] temp = new int[Data.Length * 2];
            Data.CopyTo(temp, 0);
            Data = temp;
        }

        public void PercolateDown(int hole)
        {
            int child;
            int tmp = Data[hole];

            for (; hole * 2 < CurrentSize; hole = child)
            {
                child = hole * 2;
                if (child != CurrentSize && Data[child + 1].CompareTo(Data[child]) < 0)
                    child++;

                if (Data[child].CompareTo(tmp) < 0)
                    Data[hole] = Data[child];

                else
                    break;
            }
            Data[hole] = tmp;
        }

        public int FindMin()
        {
            return Data[1];
        }
    }
}
