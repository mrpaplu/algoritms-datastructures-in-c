﻿namespace Datastructures.Trees
{
    class BinaryNode
    {
        public int Element;
        public BinaryNode Left;
        public BinaryNode Right;

        public BinaryNode(int x)
        {
            Element = x;
            Left = null;
            Right = null;
        }

        public override string ToString()
        {
            return Element + " ";
        }
    }
}
