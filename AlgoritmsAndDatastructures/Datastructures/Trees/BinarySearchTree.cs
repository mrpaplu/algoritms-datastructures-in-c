﻿using System;

namespace Datastructures.Trees
{
    class BinarySearchTree
    {
        //Opdracht 2 van AD Practicum
        //geefEenNaKleinsteElement
        public BinaryNode GeefEenNaKleinsteElement()
        {
            BinaryNode node = root;
            BinaryNode node2 = root;
            while (node.Left != null)
            {
                node2 = node;
                node = node.Left;
            }

            if (node.Right != null)
            {
                node = node.Right;
                while (node.Left != null)
                    node = node.Left;

                return node;
            }
            else
                return node2;
        }

        private BinaryNode root = null;

        public BinarySearchTree() { }

        public BinarySearchTree(int x)
        {
            root = new BinaryNode(x);
        }

        public string PrintInOrder()
        {
            return PrintInOrder(root);
        }

        public string PrintPostOrder()
        {
            return PrintPostOrder(root);
        }

        public string PrintPreOrder()
        {
            return PrintPreOrder(root);
        }

        public void Insert(int x)
        {
            root = Insert(x, root);
        }

        public void Remove(int x)
        {
            root = Remove(x, root);
        }

        public void RemoveMin()
        {
            root = RemoveMin(root);
        }

        public int? FindMin()
        {
            return ElementAt(FindMin(root));
        }

        public int? FindMinRecursive()
        {
            return ElementAt(FindMinRecursive(root));
        }

        public int? FindMax()
        {
            return ElementAt(FindMax(root));
        }

        public int? FindMaxRecursive()
        {
            return ElementAt(FindMaxRecursive(root));

        }

        public int? Find(int x)
        {
            return ElementAt(Find(x, root));
        }

        public int? FindRecursive(int x)
        {
            return ElementAt(FindRecursive(x, root));
        }

        public void MakeEmpty()
        {
            root = null;
        }

        public bool IsEmpty()
        {
            return root == null;
        }

        private string PrintInOrder(BinaryNode n)
        {
            string ret = "";

            if (n.Left != null)
            {
                ret += PrintInOrder(n.Left);
            }
            ret += n;

            if (n.Right != null)
            {
                ret += PrintInOrder(n.Right);
            }
            return ret;
        }

        private string PrintPostOrder(BinaryNode n)
        {
            string str = "";

            if (n.Left != null)
            {
                str += PrintPostOrder(n.Left);
            }

            if (n.Right != null)
            {
                str += PrintPostOrder(n.Right);
            }

            str += n;
            return str; 
        }

        private string PrintPreOrder(BinaryNode n)
        {
            string str = "";

            str += n;
            if (n.Left != null)
            {
                str += PrintPreOrder(n.Left);
            }

            if (n.Right != null)
            {
                str += PrintPreOrder(n.Right);
            }
            return str;
        }

        private int? ElementAt(BinaryNode n)
        {
            return (n == null) ? (int?)null : n.Element;
        }

        private BinaryNode Find(int x, BinaryNode t)
        {
            while (t != null)
            {
                if (x.CompareTo(t.Element) < 0)
                {
                    t = t.Left;
                }
                else if (x.CompareTo(t.Element) > 0)
                {
                    t = t.Right;
                }
                else
                {
                    return t;
                }
            }

            return null;
        }

        private BinaryNode FindRecursive(int x, BinaryNode t)
        {
            if (t != null)
            {
                if (x.CompareTo(t.Element) < 0)
                {
                    t = t.Left;
                }
                else if (x.CompareTo(t.Element) > 0)
                {
                    t = t.Right;
                }
                else
                {
                    return t;
                }

                return FindRecursive(x, t);
            }

            return null;
        }

        private BinaryNode FindMin(BinaryNode t)
        {
            if (t != null)
            {
                while (t.Left != null)
                {
                    t = t.Left;
                }
            }

            return t;
        }

        private BinaryNode FindMinRecursive(BinaryNode t)
        {
            if (t.Left == null)
            {
                return t;
            }

            return FindMinRecursive(t.Left);
        }

        private BinaryNode FindMax(BinaryNode t)
        {
            if (t != null)
            {
                while (t.Right != null)
                {
                    t = t.Right;
                }
            }

            return t;
        }

        private BinaryNode FindMaxRecursive(BinaryNode t)
        {
            if (t.Right == null)
            {
                return t;
            }

            return FindMaxRecursive(t.Right);
        }

        private BinaryNode Insert(int x, BinaryNode t)
        {
            if (t == null)
            {
                t = new BinaryNode(x);
            }
            else if (x.CompareTo(t.Element) < 0)
            {
                t.Left = Insert(x, t.Left);
            }
            else if (x.CompareTo(t.Element) > 0)
            {
                t.Right = Insert(x, t.Right);
            }
            else
            {
                throw new Exception("Duplicate item: " + x.ToString());
            }

            return t;
        }

        private BinaryNode RemoveMin(BinaryNode t)
        {
            if (t == null)
            {
                throw new Exception("Item not found");
            }
            else if (t.Left != null)
            {
                t.Left = RemoveMin(t.Left);
                return t;
            }
            else
            {
                return t.Right;
            }
        }

        private BinaryNode Remove(int x, BinaryNode t)
        {
            if (t == null)
            {
                throw new Exception("Item not found: " + x.ToString());
            }

            if (x.CompareTo(t.Element) < 0)
            {
                t.Left = Remove(x, t.Left);
            }
            else if (x.CompareTo(t.Element) > 0)
            {
                t.Right = Remove(x, t.Right);
            }
            else if (t.Left != null && t.Right != null)
            {
                t.Element = FindMin(t.Right).Element;
                t.Right = RemoveMin(t.Right);
            }
            else
            {
                t = (t.Left != null) ? t.Left : t.Right;
            }

            return t;
        }
    }
}
