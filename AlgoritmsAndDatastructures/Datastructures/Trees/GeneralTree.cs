﻿using System;

namespace Datastructures.Trees
{
    class GeneralTree<T>
    {
        public static TreeNode<string> root;
        public int Size { get; set; }

        /// <summary>
        /// Niet het mooiste.. Maar het werkt.
        /// </summary>
        public void BuildTree()
        {
            root = new TreeNode<string>();
            root.Data = "A";
            root.FirstChild = new TreeNode<string>();
            root.FirstChild.Data = "B";
            root.FirstChild.FirstChild = new TreeNode<string>();
            root.FirstChild.FirstChild.Data = "F";
            root.FirstChild.FirstChild.NextSibling = new TreeNode<string>();
            root.FirstChild.FirstChild.NextSibling.Data = "G";
            root.FirstChild.NextSibling = new TreeNode<string>();
            root.FirstChild.NextSibling.Data = "C";
            root.FirstChild.NextSibling.NextSibling = new TreeNode<string>();
            root.FirstChild.NextSibling.NextSibling.Data = "D";
            root.FirstChild.NextSibling.NextSibling.FirstChild = new TreeNode<string>();
            root.FirstChild.NextSibling.NextSibling.FirstChild.Data = "H";
            root.FirstChild.NextSibling.NextSibling.NextSibling = new TreeNode<string>();
            root.FirstChild.NextSibling.NextSibling.NextSibling.Data = "E";
            root.FirstChild.NextSibling.NextSibling.NextSibling.FirstChild = new TreeNode<string>();
            root.FirstChild.NextSibling.NextSibling.NextSibling.FirstChild.Data = "I";
            root.FirstChild.NextSibling.NextSibling.NextSibling.FirstChild.NextSibling =
                new TreeNode<string>();
            root.FirstChild.NextSibling.NextSibling.NextSibling.FirstChild.NextSibling.Data = "J";
            root.FirstChild.NextSibling.NextSibling.NextSibling.FirstChild.NextSibling.FirstChild =
                new TreeNode<string>();
            root.FirstChild.NextSibling.NextSibling.NextSibling.FirstChild.NextSibling.FirstChild
                .Data = "K";
        }

        public void PrintPreOrder(TreeNode<T> root)
        {
            if (root == null)
            {
                return;
            }
            Console.WriteLine(root.Data);
            PrintPreOrder(root.FirstChild);
            PrintPreOrder(root.NextSibling);
        }

        public int GetSize(TreeNode<T> root)
        {
            if (root == null)
            {
                return 0;
            }

            while (root.FirstChild != null)
            {
                Size++;
                GetSize(root.FirstChild);
            }
            while (root.NextSibling != null)
            {
                Size++;
                GetSize(root.NextSibling);
            }
            return Size;
        }
    }

    public class TreeNode<T>
    {
        public TreeNode<T> FirstChild { get; set; }
        public TreeNode<T> NextSibling { get; set; }
        public T Data { get; set; }
    }
}
