﻿namespace Datastructures.Trees
{
    class BinaryTree<T>
    {
        BinaryNode root;

        public static BinaryTree<string> CreateTestTree()
        {
            BinaryTree<string> tree = new BinaryTree<string>();
            tree.root = new BinaryNode(1);
            tree.root.Left = new BinaryNode(2);
            tree.root.Right = new BinaryNode(3);
            tree.root.Right.Left = new BinaryNode(4);
            tree.root.Right.Right = new BinaryNode(5);

            return tree;
        }

        public override string ToString()
        {
            if (root != null)
                return root.ToString();
            return "lege boom";
        }
    }
}
