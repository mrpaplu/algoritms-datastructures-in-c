﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP_AD_2013_S1038902_KoenClaassen
{
    class Path : IComparable<Path>
    {
        public Vertex Dest;
        public double Cost;

        public Path(Vertex d, double c)
        {
            Dest = d;
            Cost = c;
        }

        public int CompareTo(Path rhs)
        {
            double otherCost = rhs.Cost;
            return Cost < otherCost ? -1 : Cost > otherCost ? 1 : 0;
        }
    }
}
