﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP_AD_2013_S1038902_KoenClaassen
{
    class Graph
    {
        //Opdracht4b
        //vulAfstand
        public void VulAfstand(String startName)
        {
            ClearAll();

            Vertex start;
            VertexMap.TryGetValue(startName, out start);
            if (start == null)
                throw new Exception("Start vertex not found");

            Queue<Vertex> q = new Queue<Vertex>();
            q.Enqueue(start);
            start.Afstand = 0;

            while (!q.IsEmpty())
            {
                Vertex v = q.Dequeue();

                foreach (Edge item in v.Adj)
                {
                    Vertex w = item.Dest;

                    if (w.Afstand == -1)
                    {
                        w.Afstand = v.Afstand + 1;
                        w.Prev = v;
                        q.Enqueue(w);
                    }
                }
            }
        }

        //Opdracht4b Print gevulde Afstand
        //toonAfstand
        public void ToonAfstand(String dest)
        {
            foreach (Vertex v in VertexMap.Values)
            {
                Console.WriteLine("Afstand van " + v.Name + " naar " + dest + " is " + v.Afstand);
            }
        }

        //Opdracht 4c
        //hasCycle
        public bool HasCycle(String vertexName)
        {
            bool cycle = false;
            Vertex start = GetVertex(vertexName);

            List<Vertex> vList = new List<Vertex>();
            vList.Add(start);
            start.Scratch = 1;

            for (int i = 0; i < vList.Count; i++)
            {

                foreach (Edge e in vList[i].Adj)
                {
                    if (e.Dest == start)
                    {
                        cycle = true;
                        break;
                    }
                    else if (e.Dest.Scratch == 0)
                    {
                        vList.Add(e.Dest);
                        e.Dest.Scratch = 1;
                    }
                }
            }
            return cycle;
        }

        //Opdracht 4D
        //toonCycles
        public void toonCycles()
        {
            foreach (Vertex v in VertexMap.Values)
            {
                Console.WriteLine(v.Name+" has cycle: " + HasCycle(v.Name));
            }
        }


        //Contains INFINITY
        public static readonly double INFINITY = Double.MaxValue;

        //The collection wherein the vertices are contained
        private Dictionary<String, Vertex> VertexMap = new Dictionary<String, Vertex>();

        /// <summary>
        /// Add a new edge to the graph
        /// </summary>
        /// <param name="sourceName"></param>
        /// <param name="destName"></param>
        /// <param name="cost"></param>
        public void AddEdge(String sourceName, String destName/*, Double cost*/)
        {
            Vertex v = GetVertex(sourceName);
            Vertex w = GetVertex(destName);
            v.Adj.Add(new Edge(w /*, cost*/));
        }
        public void AddDoubleEdge(String sourceName, String destName/*, Double cost*/)
        {
            Vertex v = GetVertex(sourceName);
            Vertex w = GetVertex(destName);
            v.Adj.Add(new Edge(w /*, cost*/));
            w.Adj.Add(new Edge(v /*, cost*/));
        }

        /// <summary>
        /// Driver routine to handle unreachables and print total cost.
        /// It calls recursive routine to print shortest path to destNode after 
        /// a shortest path algorithm has run.
        /// </summary>
        /// <param name="destName"></param>
        public void PrintPath(String destName)
        {
            Vertex w;
            VertexMap.TryGetValue(destName, out w);
            if (w == null)
                throw new Exception("No element found");
            else if (w.Dist == INFINITY)
                Console.WriteLine(destName + " is unreachable");
            else
            {
                Console.WriteLine("(Cost is: "+w.Dist+")");
                PrintPath(w);
            }
        }

        /// <summary>
        /// Single-source unweighted shortest-path algorithm.
        /// </summary>
        /// <param name="startName"></param>
        public void Unweighted(String startName)
        {
            ClearAll();

            Vertex start;
            VertexMap.TryGetValue(startName, out start);
            if (start == null)
                throw new Exception("Start vertex not found");

            Queue<Vertex> q = new Queue<Vertex>();
            q.Enqueue(start);
            start.Dist = 0;

            while (!q.IsEmpty())
            {
                Vertex v = q.Dequeue();

                foreach (Edge item in v.Adj)
                {
                    Vertex w = item.Dest;

                    if (w.Dist == INFINITY)
                    {
                        w.Dist = v.Dist + 1;
                        w.Prev = v;
                        q.Enqueue(w);
                    }
                }
            }
        }

        ///// <summary>
        ///// Single-source weighted shortest-path algorithm
        ///// </summary>
        ///// <param name="startName"></param>
        //public void Dijkstra(String startName)
        //{
        //    BinaryHeap<Path> bh = new BinaryHeap<Path>();

        //    Vertex start;
        //    VertexMap.TryGetValue(startName, out start);

        //    if (start == null)
        //        throw new Exception("Start vertex not found");

        //    ClearAll();
        //    bh.Add(new Path(start, 0));
        //    start.Dist = 0;

        //    int nodesSeen = 0;
        //    while (!bh.IsEmpty() && nodesSeen < VertexMap.Count)
        //    {
        //        Path vrec = bh.Remove();
        //        Vertex v = vrec.Dest;

        //        if (v.Scratch != 0)
        //            continue;

        //        v.Scratch = 1;
        //        nodesSeen++;

        //        foreach (Edge e in v.Adj)
        //        {
        //            Vertex w = e.Dest;
        //            double cvw = e.Cost;

        //            if (cvw < 0)
        //                throw new Exception("Graph has negative edges");

        //            if (w.Dist > v.Dist + cvw)
        //            {
        //                w.Dist = v.Dist + cvw;
        //                w.Prev = v;
        //                bh.Add(new Path(w, w.Dist));
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Single-source negative-weighted shortest-path algorithm.
        /// </summary>
        /// <param name="startName"></param>
        public void Negative(String startName)
        {
            ClearAll();

            Vertex start;
            VertexMap.TryGetValue(startName, out start);

            if (start == null)
                throw new Exception("Start vertex not found");

            Queue<Vertex> q = new Queue<Vertex>();
            q.Enqueue(start);
            start.Dist = 0;
            start.Scratch++;

            while (!q.IsEmpty())
            {
                Vertex v = q.Dequeue();
                if (v.Scratch++ > 2 * VertexMap.Count)
                    throw new Exception("Negative cycle detected");

                foreach (Edge e in v.Adj)
                {
                    Vertex w = e.Dest;
                    double cvw = e.Cost;

                    if (w.Dist > v.Dist + cvw)
                    {
                        w.Dist = v.Dist + cvw;
                        w.Prev = v;

                        if (w.Scratch++ % 2 == 0)
                            q.Enqueue(w);
                        else
                            w.Scratch--;
                    }
                }
            }
        }

        /// <summary>
        /// If the vertex is not present, add it to VertexMap.
        /// In either case, return the Vertex.
        /// </summary>
        /// <param name="vertexName"></param>
        /// <returns></returns>
        private Vertex GetVertex(String vertexName)
        {
            Vertex v;
            VertexMap.TryGetValue(vertexName, out v);

            if (v == null)
            {
                v = new Vertex(vertexName);
                VertexMap.Add(vertexName, v);
            }

            return v;
        }

        /// <summary>
        /// Recursive routine to print shortest path to dest after running shortest path algorithm.
        /// The path is known to exist.
        /// </summary>
        /// <param name="dest"></param>
        private void PrintPath(Vertex dest)
        {
            if (dest.Prev != null)
            {
                PrintPath(dest.Prev);
                Console.WriteLine(" to ");
            }
            Console.WriteLine(dest.Name);
        }

        /// <summary>
        /// Initializes the vertex output info prior to running any shortest path algorithm.
        /// </summary>
        private void ClearAll()
        {
            foreach (Vertex item in VertexMap.Values)
            {
                item.Reset();
            }
        }

        //Used to signal violations of preconditions for various shortest path algorithms.
        class GrapException : SystemException
        {
            public GrapException(String name) : base(name) { }
        }
    }
}
