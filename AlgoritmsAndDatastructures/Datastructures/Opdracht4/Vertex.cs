﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP_AD_2013_S1038902_KoenClaassen
{
    class Vertex
    {
        //Opdracht4
        public int Afstand;

        public String Name;
        public List<Edge> Adj;
        public double Dist;
        public Vertex Prev;
        public int Scratch;

        public Vertex(String nm)
        {
            Name = nm; Adj = new List<Edge>(); //Reset();
        }

        public void Reset()
        {
            Afstand = -1;  Dist = Graph.INFINITY; Prev = null; /*Pos = null;*/ Scratch = 0;
        }
    }
}
