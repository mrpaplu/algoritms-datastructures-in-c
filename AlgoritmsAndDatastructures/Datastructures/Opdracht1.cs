﻿using System;

namespace Datastructures
{
    class Opdracht1
    {
        public void PrintLetters(int n)
        {
            if (n > 0)
            {
                Console.Write("A");
                PrintLetters(n - 1);
                Console.Write("Z");
            }
        }

        public void PrintLetters2(int p, int q)
        {
            if (p > 0)
                Console.Write("A");

            if (p > 0 || q > 0)
                PrintLetters2(p - 1, q - 1);

            if (q > 0)
                Console.Write("Z");
        }
    }
}
